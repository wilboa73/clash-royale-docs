# Compter l'élixir

## Pourquoi

* Savoir quand attaquer
* Mettre une attaque sur une tour qui ne pourra pas être défendu
* Couplé au compte des cartes, permet de contrôler totalement la partie

# 1- Connaître le coût des cartes

* Doit être connu par coeur
* Cerveau ne doit pas passer de temps à additioner
* Exemple: L'adversaire joue en même temps CHEVAUCHEUR DE COCHONS - GANG DE GOBELINS - ESPRITS DE FEU = 4 + 3 + 2 = 9

# 2- Connaître les mécaniques de l'elixir

## 1V1

* 1 elixir toutes les 2,8 secondes
* ELIXIR X2 = 1 elixir bonus
* ELIXIR X2 = 1 elixir toutes les 1,4 secondes

## 2V2

* 15% plus long - 1 elixir toutes les 3,3 secondes
* ELIXIR X2 30% plus long - 1 elixir toutes les 2s

## BONUS

* Sur 3 minutes de partie, 91 d'elixir sont utilisables

# 3 - Collecteur d'Elixir

* Produit 1 d'elixir toutes les 8,5 secondes
* Coûte 6 elixir, rapporte 8 elixir si non touché
* Si touché par boule de feu ou poison, ne produit que 2 d'elixir

# 4 - Comment compter

## Utiliser des points de repère

* Lors d'un reset (pause de 10 secondes dans la bataille) lorsque votre adversaire joue de nouveau une troupe, surtout si derrière son roi, il est TRES PROBABLEMENT à 10 d'elixir
* Lorsque vous savez l'opposant avec plus beaucoup d'elixir, si il pose une troupe au mauvais moment, par exemple, trop en retard, alors qu'elle aurait été parfaite 1 seconde avant, c'est TRES PROBABLEMENT, qu'il vient d'y mettre tout son elixir et attendait de récupérer son elixir pour la jouer => il retourne à 0

## Se baser sur son propre elixir

* Calculez la différence entre ce que vos cartes vous ont coûté et ce que les cartes de l'adversaire lui ont coûté, si vous n'avez pas fait "d'elixir flow" (perte d'elixir en restant à 10), vous pouvez savoir exactement combien l'adversaire a
Exemple : Vous êtes à 3 d'elixir, vous avez posé Prince, puis horde de minions, l'attaque n'a rien donné, vous avez ensuité posé un p.e.k.k.a sans avoir laissé l'elixir couler (5 + 5 + 7 = 17)
=> L'adversaire a défendu votre attaque avec un sorcier, puis a posé un golem en fond de cours (5 + 8 = 13)
==> Vous savez que l'adversaire a 7 d'elixir (4 de plus que vous qui en avez 3)

C'est particulièrement simple en début de partie, quand il y a moins de 10 d'elixir de joué, par exemple si vous avez défendu un push CHEVAUCHEUR DE COCHON + GOLEM DE GLACE avec votre bucheron, vous savez que vous avez 2 d'elixir de plus

# 5 - Comment réussir à l'appliquer ?

* Comme pour absolument tout, ENTRAINEMENT, ENTRAINEMENT et ENTRAINEMENT
* Entraînez-vous dans un mode où ce n'est pas grave pour vous si vous perdez, pour certains le ladder, pour d'autres le défi classique,... A votre guise ;)
* Utilisez le REPLAY pour vérifier si vous aviez raison, comptez, jouez, et vérifiez après au calme avec un chocolat chaud si vous aviez raison ou de combien vous vous trompiez